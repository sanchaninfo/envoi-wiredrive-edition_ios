  /*
    File Name   :      index.js
    Project     :      DameDashStudios IOS App
    Copyright (c)      http://www.damedashstudios.com
    author      :      Prasanna
    license     :   
    version     :      0.0.1  
    Created on  :      Aug 23, ‎2016
    Last modified on:  August 23, ‎2016 
    Description :      
     Organisation:     Peafowl inc.  
    */

var app = {
    // Application Constructor
    initialize: function() {
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'app.receivedEvent(...);'
    onDeviceReady: function() {
        
        app.receivedEvent('deviceready');

        // window.location.href = 'dashboard.html';
        var myDB = window.sqlitePlugin.openDatabase({name: 'DameDashDB.db', location: 'default'});
        myDB.transaction(function(transaction) {
                         transaction.executeSql('CREATE TABLE IF NOT EXISTS damedashSettings (id integer primary key, key1 text, key2 text, key3 text,key4 text,key5 text,key6 text)', [],
                                                function(tx, result) {
                                                
                                                },
                                                function(error) {
                                                alert("Error occurred while creating the table.:" + error);
                                                //$("#alertMsg").html('<div class="alert_blk"></div>');
                                                });
                         });

    },
    // Update DOM on a Received Event
    receivedEvent: function(id) {
        var parentElement = document.getElementById(id);
        var listeningElement = parentElement.querySelector('.listening');
        var receivedElement = parentElement.querySelector('.received');

        listeningElement.setAttribute('style', 'display:none;');
        receivedElement.setAttribute('style', 'display:block;');

        console.log('Received Event: ' + id);
    }
};



app.initialize();

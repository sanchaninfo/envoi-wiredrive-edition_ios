  /*
            File Name   :      app.js
            Project     :      DameDashStudios IOS App
            Copyright (c)      http://www.damedashstudios.com
            author      :      Prasanna 
            license     :   
            version     :      0.0.1  
            Created on  :      Aug 22, ‎2016
            Last modified on:  August ‎22, ‎2016 
            Description :      This file contains following functions 
                                 * Create sqlite table if not exist.
                                 * Open Auth0 popup when Join button click
                                 * Delete login user details from settings table when user clicks on logout button. 
            Organisation:      Peafowl inc.  
            */


  document.addEventListener('deviceready', function() {
        
      // Creating damedashSettings Table if not exist 
     // var myDB = window.openDatabase("Database", "1.0", "DameDashDB", -1);
     var myDB = window.sqlitePlugin.openDatabase({name: 'DameDashDB.db', location: 'default'});
      myDB.transaction(function(transaction) {
          transaction.executeSql('CREATE TABLE IF NOT EXISTS damedashSettings (id integer primary key, key1 text, key2 text, key3 text,key4 text,key5 text,key6 text)', [],
              function(tx, result) {
               
              },
              function(error) {
                   alert("Error occurred while creating the table.:" + error);
                  //$("#alertMsg").html('<div class="alert_blk"></div>');
              });
      });


     /* myDB.transaction(function(transaction) {

          transaction.executeSql('SELECT * FROM damedashSettings', [], function(tx, results) {
              var len = results.rows.length,
                  i;
                                console.log("user "+results.rows.item(0).key1)
              if (len > 0) {
                                 window.location.href = 'dashboard.html';
                  for (i = 0; i < len; i++) {
                                 
                      
                  }
              }
                else{
                    //setTimeout(function(){window.location.href = 'pairing.html';},6000);
                   
                    
                }
          }, null);
      });*/


      // Join/Login

//      var lock = new Auth0Lock(
//          // All these properties are set in auth0-variables.js
//          AUTH0_CLIENT_ID,
//          AUTH0_DOMAIN
//      );
//      var userProfile;
//      $('.signinbtn').click(function(e) {
//          showLock(e);
//      });
//      $('.freebtn').click(function(e) {
//          showLock(e);
//      });
//      $('.fJoin').click(function(e) {
//          showLock(e);
//      });
//      $('.tablebtn').click(function(e) {
//          showLock(e);
//      });
//
//      function showLock(e) {
//          e.preventDefault();
//          lock.show(function(err, profile, token) {
//              $("#loading").show();
//              $(".mainwrapper").hide();
//                  if (err) {
//                  // Error callback
//                  $("#loading").hide();
//                  //$(".mainwrapper").show();
//                  alert('There was an error logging in');
//                   lock.hide();
//                  $(".mainwrapper").show();
//              } else {
//                  var myDB = window.sqlitePlugin.openDatabase({name: 'DameDashDB.db', location: 'default'}, null, null);
//                  myDB.transaction(function(tx) {
//                          var qry = "DELETE FROM damedashSettings";
//                          tx.executeSql(qry, function() {
//                              // console.log("Delete success");
//                          });
//                      }, function errorCB(err) {
//                           showAlert("Error processing SQL: " + err.code);
//                          $("#loading").hide();
//                          lock.hide();
//                          $(".mainwrapper").show();
//                      },
//                      function() {
//
//                          myDB.transaction(function(transaction) {
//                              var executeQuery = "INSERT INTO damedashSettings (key1, key2,key4) VALUES (?,?,?)";
//                              transaction.executeSql(executeQuery,  function(tx, result) {
//                                      //console.log('Inserted:' + result);
//                                      $.ajax({
//                                          // url: "http://192.168.43.158:9000/mobileSignIn",
//                                          url: ServerUrl + "mobileSignIn",
//                                          type: "POST",
//                                          data: JSON.stringify({
//                                              "mobileSignIn": {
//                                                  "token": token,
//                                                  "id": deviceID
//                                              }
//                                          }),
//                                          dataType: "json",
//                                          contentType: "application/json; charset=utf-8",
//                                          traditional: true,
//                                          success: function(data) {
//                                               UpdateID(data.accountResult._id);
//                                          },
//                                          error: function(jqXHR, textStatus, errorThrown) {
//                                             showAlert(errorThrown);
//                                               $("#loading").hide();
//                                              lock.hide();
//                                              $(".mainwrapper").show();
//                                          },
//                                          complete: function() {
//
//                                          }
//                                      });
//                                  },
//                                  function(error) {
//                                       showAlert('Error occurred:' + error);
//                                      $("#loading").hide();
//                                      lock.hide();
//                                      $(".mainwrapper").show();
//                                  });
//                          });
//
//                      });
//
//              }
//          });
//      }


  }, false);



  function UpdateID(id) {

      var myDB = window.sqlitePlugin.openDatabase({name: 'DameDashDB.db', location: 'default'}, null, null);
      myDB.transaction(function(tx) {
          var qry = "UPDATE damedashSettings SET key3='" + id + "'";
          tx.executeSql(qry, function() {

          });
      }, function errorCB(err) {
            showAlert("Error processing SQL: " + err.code);
          $("#loading").hide();
      }, function() {
          window.location.href = 'dashboard.html';
      });

  }
